#!/usr/bin/python

import sys

count = 0
oldKey = None
mostVisitedKey = None
mostVisitedCount = 0

for line in sys.stdin:
    data_mapped = line.strip().split("\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue

    thisKey, thisCount = data_mapped

    if oldKey and oldKey != thisKey:
        if count > mostVisitedCount:
            mostVisitedKey, mostVisitedCount = oldKey, count
        oldKey = thisKey;
        count = 0

    oldKey = thisKey
    count += int(thisCount)

if oldKey != None:
    if count > mostVisitedCount:
        mostVisitedKey, mostVisitedCount = oldKey, count

print mostVisitedKey, "\t", mostVisitedCount
