#!/usr/bin/python

import re
import sys

prefix = "http://www.the-associates.co.uk"
regex = re.compile('([^ ]*) ([^ ]*) ([^ ]*) \[([^]]*)\] "([^ ]*) ([^ ]*) ([^"]*)" ([^ ]*) ([^ ]*)')

for line in sys.stdin:
    data = line.strip().split("\t")
    m = regex.match(line)
    if m:
        host, ignore, user, date, http_method, path, protocol, status, size = m.groups()
        if path.startswith(prefix):
            path = path[len(prefix):]
        print "{0}\t1".format(path)
    else:
        print "error-line##{0}\t1".format(line)

